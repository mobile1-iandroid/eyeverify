//
//  SwitchTableViewCell.swift
//  EyeprintID
//
//  Created by goodle on 6/14/15.
//  Copyright (c) 2015 EyeVerify. All rights reserved.
//

import UIKit

class SwitchTableViewCell: UITableViewCell {

    var switchView: UISwitch?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addSwitch()
        //        aCell.selectionStyle = UITableViewCellSelectionStyleNone;
        //        UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
        //        aCell.accessoryView = switchView;
        //        [switchView setOn:NO animated:NO];
        //        [switchView addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        //        [switchView release];
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForInterfaceBuilder() {
        addSwitch()
    }
    
    func addSwitch() {
        self.selectionStyle = UITableViewCellSelectionStyle.None
        switchView = UISwitch()
        self.accessoryView = switchView
        switchView?.on = false
    }
}
