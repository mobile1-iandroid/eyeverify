//===-- EnrollResultViewController.swift - EnrollResultViewController class ----------------------------===//
//
//                     EyeVerify Codebase
//
//===----------------------------------------------------------------------===//
///
/// @file
/// @brief This file contains the declaration of the EnrollResultViewController class.
///
//===----------------------------------------------------------------------===//

import UIKit

class VerifyResultViewController: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    var message: String?
    var messageColor: UIColor?
    @IBOutlet weak var doneButton: UIButton!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        messageLabel.textColor = messageColor
        messageLabel.text = message
        
        if let demoMode = NSUserDefaults.standardUserDefaults().objectForKey("demoMode") as? Bool {
            if demoMode {
                if let theme = EVThemeController.currentTheme {
                    self.doneButton.tintColor = theme.globalTintColor
                    self.navigationController?.navigationBar.tintColor = theme.globalTintColor
                }
            }
        }
    }
}
