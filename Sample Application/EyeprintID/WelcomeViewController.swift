//
//  WelcomeViewController.swift
//  EyeVerify
//
//  Created by goodle on 6/11/15.
//  Copyright (c) 2015 EyeVerify. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var getStartedButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        if let demoMode = NSUserDefaults.standardUserDefaults().objectForKey("demoMode") as? Bool {
            if demoMode {
                if let theme = EVThemeController.currentTheme {
                    self.getStartedButton.tintColor = theme.globalTintColor
                    self.navigationController?.navigationBar.tintColor = theme.globalTintColor
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
