//===-- UserViewController.swift - UserViewController class ----------------------------===//
//
//                     EyeVerify Codebase
//
//===----------------------------------------------------------------------===//
///
/// @file
/// @brief This file contains the declaration of the UserViewController class.
///
//===----------------------------------------------------------------------===//

import UIKit

class UserViewController: UIViewController, UITextFieldDelegate, UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate {
    
    let hintCellHeight = 40
    let hintMaxHeight = 120
    let cellIdentifier = "HintCell"
    
    var allUsers: [String] = []
    var matchingUsers: [String] = []
    
    @IBOutlet weak var UserHintsTableHeight: NSLayoutConstraint!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var enrollButton: UIButton!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var settingsButton: UIBarButtonItem!
    @IBOutlet weak var userHintsTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String
        userTextField.text = NSUserDefaults.standardUserDefaults().objectForKey("user") as? String
        
//        let ev = EyeVerify.sharedInstance.getEyeVerify()
//        var users = ev?.getEnrolledUsers()
//        
//        if let users = users {
//            if users.count > 0 {
//                println("List of currently enrolled users: ")
//                for user in users {
//                    println("   \(user)")
//                }
//            } else {
//                println("No users are currently enrolled on this device")
//            }
//        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let demoMode = NSUserDefaults.standardUserDefaults().objectForKey("demoMode") as? Bool {
            if demoMode {
                if let theme = EVThemeController.currentTheme {
                    self.enrollButton.tintColor = theme.globalTintColor
                    self.verifyButton.tintColor = theme.globalTintColor
                    self.settingsButton.tintColor = theme.globalTintColor
                    self.navigationController?.navigationBar.tintColor = theme.globalTintColor
                }
            } else {
                self.navigationController?.navigationBar.tintColor = nil
                self.enrollButton.tintColor = nil
                self.verifyButton.tintColor = nil
                self.settingsButton.tintColor = nil
            }
        }
        
        userHintsTable.reloadData()
        
        let ev = EyeVerifyLoader.getEyeVerifyInstance()
        if let users = ev?.getEnrolledUsers() as? [String] {
            allUsers = users
        }
        self.checkUser()
        self.reloadMatches(nil)
    }
    
    override func viewDidAppear(animated: Bool) {
//        if let license =  NSUserDefaults.standardUserDefaults().objectForKey("license") as? String {
//            if license.isEmpty {
//                self.performSegueWithIdentifier("license", sender:self)
//            }
//        } else {
//            self.performSegueWithIdentifier("license", sender:self)
//        }
        self.checkUser()
        self.reloadMatches(nil)
    }
    
    @IBAction func verify(sender: AnyObject) {
        let currSysVer: NSString = UIDevice.currentDevice().systemVersion
        if currSysVer.floatValue < 8.0 {
            let message = UIAlertView(title: NSLocalizedString("iOS Version Too Old", comment: ""), message: NSLocalizedString("EyeVerify can only be used on iOS 8 or later", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("OK", comment: ""))
            message.show()
            return
        }
        if !(self.validateInput()) {
            return
        }
        NSUserDefaults.standardUserDefaults().setObject(self.userTextField.text, forKey: "username")
        NSUserDefaults.standardUserDefaults().synchronize()
        self.performSegueWithIdentifier("verify", sender: self)
    }
    
    @IBAction func enroll(sender: AnyObject) {
        let currSysVer: NSString = UIDevice.currentDevice().systemVersion
        if currSysVer.floatValue < 8.0 {
            let message = UIAlertView(title: NSLocalizedString("iOS Version Too Old", comment: ""), message: NSLocalizedString("EyeVerify can only be used on iOS 8 or later", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("OK", comment: ""))
            message.show()
            return
        }
        if !(self.validateInput()) {
            return
        }
        NSUserDefaults.standardUserDefaults().setObject(self.userTextField.text, forKey: "username")
        NSUserDefaults.standardUserDefaults().synchronize()
        self.performSegueWithIdentifier("enroll", sender: self)
    }
    
    @IBAction func unwindToUserViewController(unwindSegue: UIStoryboardSegue) {}
    
    // MARK: - User Hints Table Methods
    func validateInput() -> Bool {
        if let userName = self.userTextField.text {
            if userName.isEmpty {
                let message = UIAlertView(title: NSLocalizedString("Invalid Username", comment: ""), message: NSLocalizedString("Username should consist of at least one alphanumeric character", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("OK", comment:""))
                message.show()
                return false
            }
            NSUserDefaults.standardUserDefaults().setObject(userName, forKey: "user")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        userTextField.resignFirstResponder()
        return true
    }
    
    func checkUser() {
        if let ev = EyeVerifyLoader.getEyeVerifyInstance() {
            var isEnrolled = false
            let userName = userTextField.text
            if !(userName.isEmpty) {
                isEnrolled = ev.isUserEnrolled(userName)
            }
            
            if isEnrolled {
                enrollButton.setTitle(NSLocalizedString("Recreate Eyeprint", comment: ""), forState: UIControlState.Normal)
            } else {
                enrollButton.setTitle(NSLocalizedString("Create an Eyeprint", comment: ""), forState: UIControlState.Normal)
            }
            
            verifyButton.enabled = isEnrolled
            verifyButton.alpha = isEnrolled ? 1.0 : 0.5
        } else {
            println("Couldn't find instance of EyeVerify")
        }
    }
    
    func reloadMatches(userName: String?) {
        var filteredUsers: [String] = []
        if let name = userName {
            if !(name.isEmpty) {
                for user in allUsers {
                    if user.hasPrefix(name) {
                        filteredUsers.append(user)
                    }
                }
            }
        }
        
        matchingUsers = filteredUsers
        let totalUsers = filteredUsers.count
        
        dispatch_async(dispatch_get_main_queue()) {
            if totalUsers != 0 {
                self.userHintsTable.reloadData()
                self.UserHintsTableHeight.constant = CGFloat(min(self.hintCellHeight * totalUsers, self.hintMaxHeight))
                self.userHintsTable.setNeedsLayout()
                self.userHintsTable.layoutIfNeeded()
            }
            self.userHintsTable.hidden = (totalUsers <= 0) || !(self.userTextField.isFirstResponder())
        }
    }
    
    @IBAction func userTextFieldChanged(sender: AnyObject) {
        self.checkUser()
        
        if let userName = userTextField.text {
            if !(userName.isEmpty) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    self.reloadMatches(userName)
                }
            }
        }
    }
    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if let userName: NSString = textField.text {
            let userNameLength = userName.length
            let theString: NSString = string
            if string.isEmpty {
                return true
            } else if theString.length > 1 {
                print("Trying to replace more than 1 character")
                return false
            } else if userNameLength >= 40 {
                print("Max userName length is 40 characters")
                return false
            }
            
            if let _ = string.rangeOfString("[A-Za-z0-9]", options: .RegularExpressionSearch) {
                return true
            } else {
                return false
            }
        }
        return false
    }
    
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingUsers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell
#if SWIFT2
//        if let theCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) {
//            cell = theCell
//        } else {
//            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
//        }
#else
    if let theCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? UITableViewCell {
        cell = theCell
    } else {
        cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
    }
#endif
        return cell
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let height = CGFloat(hintCellHeight)
        return height
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let _ = tableView.cellForRowAtIndexPath(indexPath)
    }
    
    // MARK: - Alert View Delegate Methods
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        let title = alertView.title
        if title == NSLocalizedString("License", comment: "") {
            if buttonIndex == 0 {
                // TODO: Not sure what this is doing. Should probably be removed.
                exit(0)
            } else {
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "AcceptedEULA")
            }
        } else if title == NSLocalizedString("Recreate Eyeprint", comment: "") {
            if buttonIndex == 1 {
                self.performSegueWithIdentifier("enroll", sender:self)
            }
        }
    }
}
