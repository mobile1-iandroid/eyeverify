//
//  ThemesTableViewController.swift
//  EyeprintID
//

import UIKit

class ThemesTableViewController: UITableViewController {
    
    var cellRowForEditing: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        if let demoMode = NSUserDefaults.standardUserDefaults().objectForKey("demoMode") as? Bool {
            if demoMode {
                if let theme = EVThemeController.currentTheme {
                    self.navigationItem.titleView?.tintColor = theme.globalTintColor
                    self.navigationItem.backBarButtonItem?.tintColor = theme.globalTintColor
                    self.navigationItem.rightBarButtonItem?.tintColor = theme.globalTintColor
                }
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData()
        self.navigationController?.setToolbarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.setToolbarHidden(true, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return EVThemeController.themes.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("themeCell", forIndexPath: indexPath) as? UITableViewCell {
            var theme = EVThemeController.themes[indexPath.row]
            if let currentTheme = EVThemeController.currentTheme {
                if theme.name == currentTheme.name {
                    cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                } else {
                    cell.accessoryType = UITableViewCellAccessoryType.None
                }
            } else {
                cell.accessoryType = UITableViewCellAccessoryType.None
            }
            cell.textLabel?.text = theme.name
            return cell
        }
        return UITableViewCell()
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = UITableViewCellAccessoryType.Checkmark
        let theme = EVThemeController.themes[indexPath.row]
        EVThemeController.currentTheme = theme
        self.tableView.reloadData()
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let theme = EVThemeController.themes[indexPath.row]
            if let currentTheme = EVThemeController.currentTheme {
                if theme.name == currentTheme.name {
                    let message = UIAlertView(title: NSLocalizedString("Hold up", comment: ""), message: NSLocalizedString("Please change your currently selected theme first", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("Got it", comment:""))
                    message.show()
                    return
                }
            }
            EVThemeController.deleteTheme(theme)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let row = self.cellRowForEditing {
            if let vc = segue.destinationViewController as? ThemeViewController {
                vc.theme = EVThemeController.themes[row]
                self.cellRowForEditing = nil
            }
        }
    }
    
    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        if tableView.editing {
            self.cellRowForEditing = indexPath.row
            self.performSegueWithIdentifier("edit", sender: self)
        }
    }
    
    @IBAction func unwindToThemesTableViewController(unwindSegue: UIStoryboardSegue) {}
}
