//===-- LicenseViewController.swift - LicenseViewController class ----------------------------===//
//
//                     EyeVerify Codebase
//
//===----------------------------------------------------------------------===//
///
/// @file
/// @brief This file contains the declaration of the LicenseViewController class.
///
//===----------------------------------------------------------------------===//

import UIKit

class LicenseViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var licenseTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var requestLicenseButton: UIButton!
    @IBOutlet weak var learnMoreButton: UIButton!
    
    // MARK: ViewController Callbacks
    override func viewDidLoad() {
        super.viewDidLoad()
        versionLabel.text = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String
        
        if let license =  NSUserDefaults.standardUserDefaults().objectForKey("license") as? String {
            licenseTextField.text = license
        }
        
        #if DEBUG
            licenseTextField.text = "EyeVerify-DEMO"
        #endif
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let theme = EVThemeController.currentTheme {
            self.submitButton.tintColor = theme.globalTintColor
            self.requestLicenseButton.tintColor = theme.globalTintColor
            self.learnMoreButton.tintColor = theme.globalTintColor
        }
    }
    
    // MARK: Custom
    @IBAction func submit(sender: AnyObject) {
        validateLicense(licenseTextField.text)
    }
    
    func validateLicense(license: NSString) {
        if license.length < 1 {
            let message = UIAlertView(title: NSLocalizedString("Invalid Group ID", comment: ""), message: NSLocalizedString("Group ID is required", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("OK", comment: ""))
            message.show()
            return
        }
        
        let evHelper = EyeVerify.sharedInstance
        evHelper.saveLicense(license as String)
        
        if let ev = evHelper.getEyeVerify() {
            var error: NSError?
            let compliant = ev.checkCompliance("iOS Validate License", error: &error)
            if !compliant {
                if let theError = error {
                    evHelper.saveLicense("")
                    
                    var message : UIAlertView?
                    
                    switch theError.code {
                    case EVSystemError:
                        message = UIAlertView(title: NSLocalizedString("System Error", comment: ""), message: theError.localizedDescription, delegate: nil,
                            cancelButtonTitle: NSLocalizedString("OK", comment: ""))
                    case EVUnsupportedDeviceError:
                        message = UIAlertView(title: NSLocalizedString("Device Not Supported", comment: ""), message: theError.localizedDescription, delegate: nil,
                            cancelButtonTitle: NSLocalizedString("OK", comment: ""))
                    case EVInvalidLicenseError:
                        message = UIAlertView(title: NSLocalizedString("Invalid Group ID", comment: ""), message: theError.localizedDescription, delegate: nil,
                            cancelButtonTitle: NSLocalizedString("OK", comment: ""))
                    default:
                        break
                    }
                    
                    message?.show()
                
                }
                return
            }
            self.performSegueWithIdentifier("unwindToUserViewController", sender:self)
        }
    }
    
    @IBAction func requestLicenseClicked(sender: AnyObject) {
        if let url = NSURL(string: "http://eyeverify.com/groupID") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    @IBAction func learnMore(sender: AnyObject) {
        if let url = NSURL(string: "http://www.eyeverify.com/learn-more") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
