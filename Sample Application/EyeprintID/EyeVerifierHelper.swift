//===-- EyeVerifierHelper.swift - EyeVerifierHelper class ----------------------------===//
//
//                     EyeVerify Codebase
//
//===----------------------------------------------------------------------===//
///
/// @file
/// @brief This file contains the declaration of the EyeVerifierHelper class.
///
//===----------------------------------------------------------------------===//

import UIKit

enum ExampleMode : Int {
    case Basic // Without user key storage
    case LocalWithoutKeyGeneration // Local without user key generation
    case LocalWithKeyGeneration // Local with user key generation
}

class EyeVerifierHelper: NSObject {
    
    // Change this value to test different modes for using EyeVerify
    let mode = ExampleMode.LocalWithoutKeyGeneration
    
    var userName: String?
    
    private var eyeverifier: EyeVerifier?
    
    // EyeVerifierHelper singleton
    class var sharedInstance : EyeVerifierHelper {
    struct Static {
        static let instance : EyeVerifierHelper = EyeVerifierHelper()
        }
        return Static.instance
    }
    
    func getEyeVerifier() -> EyeVerifier? {
        initializeEyeVerifier()
        return eyeverifier
    }
    
    func saveLicense(license: String) {
        NSUserDefaults.standardUserDefaults().setObject(license, forKey: "license")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func initializeEyeVerifier() {
        if let license = NSUserDefaults.standardUserDefaults().objectForKey("license") as? String {            
            if eyeverifier != nil {
                eyeverifier!.licenseCertificate = license
                return
            } else {
                eyeverifier = EyeVerifier(license: license)
            }
        } else {
            println("Error: License not found")
        }
    }
}
