//
//  EVTheme.swift
//  EyeprintID
//

import UIKit

class EVTheme: NSObject, NSCoding {
    var name: String
    var globalTintColor: UIColor
    var primaryResultScreen: UIImage
    var detailResultScreen: UIImage
    
    init(name: String, color: UIColor, primary: UIImage, detail: UIImage) {
        self.name = name
        self.globalTintColor = color
        self.primaryResultScreen = primary
        self.detailResultScreen = detail
        super.init()
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.globalTintColor, forKey: "globalTintColor")
        aCoder.encodeObject(self.primaryResultScreen, forKey: "primaryResultScreen")
        aCoder.encodeObject(self.detailResultScreen, forKey: "detailResultScreen")
        aCoder.encodeObject(self.name, forKey: "name")
    }
    
#if SWIFT2
//    required init?(coder aDecoder: NSCoder) {
//        if let globalTintColor = aDecoder.decodeObjectForKey("globalTintColor") as? UIColor {
//            self.globalTintColor = globalTintColor
//        } else {
//            self.globalTintColor = UIColor()
//        }
//        if let primaryResultScreen = aDecoder.decodeObjectForKey("primaryResultScreen") as? UIImage {
//            self.primaryResultScreen = primaryResultScreen
//        } else {
//            self.primaryResultScreen = UIImage()
//        }
//        if let detailResultScreen = aDecoder.decodeObjectForKey("detailResultScreen") as? UIImage {
//            self.detailResultScreen = detailResultScreen
//        } else {
//            self.detailResultScreen = UIImage()
//        }
//        if let name = aDecoder.decodeObjectForKey("name") as? String {
//            self.name = name
//        } else {
//            self.name = ""
//        }
//        super.init()
//    }
#else
    required init(coder aDecoder: NSCoder) {
        if let globalTintColor = aDecoder.decodeObjectForKey("globalTintColor") as? UIColor {
            self.globalTintColor = globalTintColor
        } else {
            self.globalTintColor = UIColor()
        }
        if let primaryResultScreen = aDecoder.decodeObjectForKey("primaryResultScreen") as? UIImage {
            self.primaryResultScreen = primaryResultScreen
        } else {
            self.primaryResultScreen = UIImage()
        }
        if let detailResultScreen = aDecoder.decodeObjectForKey("detailResultScreen") as? UIImage {
            self.detailResultScreen = detailResultScreen
        } else {
            self.detailResultScreen = UIImage()
        }
        if let name = aDecoder.decodeObjectForKey("name") as? String {
            self.name = name
        } else {
            self.name = ""
        }
        super.init()
    }
#endif
}
