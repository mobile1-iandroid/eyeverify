//
//  DemoPrimaryViewController.swift
//  EyeprintID
//
//  Created by goodle on 6/14/15.
//  Copyright (c) 2015 EyeVerify. All rights reserved.
//

import UIKit

class DemoPrimaryViewController: UIViewController {

    @IBOutlet weak var primaryScreenshot: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let theme = EVThemeController.currentTheme {
            primaryScreenshot.image = theme.primaryResultScreen
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
