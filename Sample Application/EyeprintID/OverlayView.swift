//
//  OverlayView.swift
//  EyeprintID
//

import UIKit

class OverlayView: UIView {
    override func drawRect(rect: CGRect) {
        OverlayStyleKit.drawOverlay(frame: self.bounds)
    }
}
