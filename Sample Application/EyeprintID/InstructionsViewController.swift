//
//  InstructionsViewController.swift
//  EyeVerify
//
//  Created by goodle on 6/10/15.
//  Copyright (c) 2015 EyeVerify. All rights reserved.
//

import UIKit

class InstructionsViewController: UIViewController {
    
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let demoMode = NSUserDefaults.standardUserDefaults().objectForKey("demoMode") as? Bool {
            if demoMode {
                if let theme = EVThemeController.currentTheme {
                    self.startButton.tintColor = theme.globalTintColor
                    self.navigationController?.navigationBar.tintColor = theme.globalTintColor                }
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
        if let vc = segue.destinationViewController as? VerifyResultViewController {
            vc.messageColor = UIColor.blackColor()
            vc.message = NSLocalizedString("Enrollment Complete", comment: "")
        }
    }
}
