//
//  ScanningOverlayView.h
//  EyeVerifyFrameworks
//
//  Created by Joel Teply on 2/12/15.
//  Copyright (c) 2015 EyeVerify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanningOverlayView : UIView

@property (assign, nonatomic) bool targetHighlighted;

- (void) setDistanceClose;
- (void) setDistanceFar;

@end

@interface Scanline : UIView

- (void) startAnimating;
- (void) stopAnimating;

@end