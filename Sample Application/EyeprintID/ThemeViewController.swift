//
//  ThemeViewController.swift
//  EyeprintID
//

import UIKit

class ThemeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, FCColorPickerViewControllerDelegate {

    var theme: EVTheme?
    
    @IBOutlet weak var themeNameTextField: UITextField!
    @IBOutlet weak var globalTintColorView: UIView!
    
    @IBOutlet weak var primaryScreenShotImage: UIImageView!
    @IBOutlet weak var detailScreenshotImage: UIImageView!
    
    let imagePicker = UIImagePickerController()
    
    enum ResultScreenshot {
        case Primary
        case Detail
    }
    
    var screenshot: ResultScreenshot?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let theme = self.theme {
            themeNameTextField.text = theme.name
            globalTintColorView.backgroundColor = theme.globalTintColor
            primaryScreenShotImage.image = theme.primaryResultScreen
            detailScreenshotImage.image = theme.detailResultScreen
        }
        
        imagePicker.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func chooseColor(sender: AnyObject) {
        let colorPickerVC = FCColorPickerViewController()
        colorPickerVC.color = globalTintColorView.backgroundColor
        colorPickerVC.delegate = self
        
        colorPickerVC.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        self.presentViewController(colorPickerVC, animated: true, completion: nil)
    }
    
    @IBAction func pickPrimaryScreenshot(sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        screenshot = ResultScreenshot.Primary
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func pickDetailScreenshot(sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        screenshot = ResultScreenshot.Detail
        presentViewController(imagePicker, animated: true, completion: nil)
    }

    @IBAction func saveTheme(sender: AnyObject) {
        if let primary = primaryScreenShotImage.image {
            if let detail = detailScreenshotImage.image {
                if let color = globalTintColorView.backgroundColor {
                    if let name = themeNameTextField.text {
                        let theme = EVTheme(name: name, color: color, primary: primary, detail: detail)
                        self.theme = theme
                        EVThemeController.saveTheme(theme)
                    }
                }
            }
        }
        self.performSegueWithIdentifier("unwindToThemesTableViewController", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if let vc = segue.destinationViewController as? ThemesTableViewController {
//            if let theme = self.theme {
//                vc.themes.append(theme)
//            }
//        }
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
#if SWIFT2
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: AnyObject]) {
//        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
//            if let screenshot = self.screenshot {
//                switch screenshot {
//                case .Primary:
//                    primaryScreenShotImage.contentMode = .ScaleAspectFit
//                    primaryScreenShotImage.image = pickedImage
//                case .Detail:
//                    detailScreenshotImage.contentMode = .ScaleAspectFit
//                    detailScreenshotImage.image = pickedImage
//                }
//            }
//        }
//        dismissViewControllerAnimated(true, completion: nil)
//    }
#else
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if let screenshot = self.screenshot {
                switch screenshot {
                case .Primary:
                    primaryScreenShotImage.contentMode = .ScaleAspectFit
                    primaryScreenShotImage.image = pickedImage
                case .Detail:
                    detailScreenshotImage.contentMode = .ScaleAspectFit
                    detailScreenshotImage.image = pickedImage
                }
            }
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
#endif
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - FCColorPickerViewControllerDelegate Methods
    
    func colorPickerViewController(colorPicker: FCColorPickerViewController!, didSelectColor color: UIColor!) {
        globalTintColorView.backgroundColor = color
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func colorPickerViewControllerDidCancel(colorPicker: FCColorPickerViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
