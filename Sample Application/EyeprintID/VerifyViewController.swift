//
//  VerifyViewController.swift
//  EyeprintID
//

import UIKit
import AVFoundation

class VerifyViewController: UIViewController, EVAuthSessionDelegate {
    
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var scanningOverlay: ScanningOverlayView!
    
    var userKey: NSData?
    var errorMessage: String?
    var verifiedPrompt: AVAudioPlayer? = nil
    
    var shouldDismiss = false
    var verificationComplete = false
    
#if SWIFT2
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        do {
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
//        } catch _ {
//        }
//        
//        if let audioPath = NSBundle.mainBundle().pathForResource("verified_prompt", ofType: "mp3") {
//            do {
//                if let url = NSURL(string: audioPath) {
//                    verifiedPrompt = try AVAudioPlayer(contentsOfURL: url)
//                }
//            } catch _ {
//                verifiedPrompt = nil
//            }
//            if let player = verifiedPrompt {
//                player.prepareToPlay()
//            } else {
//                print("Verified prompt AVAudioPlayer not initialized!")
//            }
//            if let ev = EyeVerifyLoader.getEyeVerifyInstance() {
//                if let username =  NSUserDefaults.standardUserDefaults().objectForKey("username") as? String {
//                    if username.isEmpty {
//                        self.performSegueWithIdentifier("unwindToUserViewController", sender:self)
//                    }
//                    ev.setUserName(username)
//                } else {
//                    self.performSegueWithIdentifier("unwindToUserViewController", sender:self)
//                }
//            }
//        }
//    }
#else
    override func viewDidLoad() {
        super.viewDidLoad()
        AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient, error: nil)
        if let audioPath = NSBundle.mainBundle().pathForResource("verified_prompt", ofType: "mp3") {
            if let url = NSURL(string: audioPath) {
                verifiedPrompt = AVAudioPlayer(contentsOfURL: url, error: nil)
            }
            if let player = verifiedPrompt {
                player.prepareToPlay()
            } else {
                print("Verified prompt AVAudioPlayer not initialized!")
            }
            if let ev = EyeVerifyLoader.getEyeVerifyInstance() {
                if let username =  NSUserDefaults.standardUserDefaults().objectForKey("username") as? String {
                    if username.isEmpty {
                        self.shouldDismiss = true
                    }
                    ev.userName = username
                    ev.setCaptureView(self.captureView)
                } else {
                    self.shouldDismiss = true
                }
            } else {
                self.shouldDismiss = true
            }
        }
    }
#endif
    
    override func viewWillAppear(animated: Bool) {
        if let demoMode = NSUserDefaults.standardUserDefaults().objectForKey("demoMode") as? Bool {
            if demoMode {
                if let theme = EVThemeController.currentTheme {
                    self.cancelButton.tintColor = theme.globalTintColor
                    self.navigationController?.navigationBar.tintColor = theme.globalTintColor
                }
            } else {
                self.navigationController?.navigationBar.tintColor = nil
                self.cancelButton.tintColor = nil
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        if self.shouldDismiss {
            self.performSegueWithIdentifier("unwindToUserViewController", sender: self)
            return
        }
        if !verificationComplete {
            self.verify()
        }
    }

    func verify() {
        if let ev = EyeVerifyLoader.getEyeVerifyInstance() {
            ev.setEVAuthSessionDelegate(self)
            print("ExampleMode.LocalWithoutKey")
            ev.verifyUser(ev.userName, localCompletionBlock: { (verified, userKey, error) in
                self.verificationComplete = true
                self.userKey = userKey
                self.errorMessage = error?.localizedFailureReason
                
                var key: NSString = "nil"
                if let userKeyData = userKey {
                    if let theKey = NSString(data: userKeyData, encoding: NSUTF8StringEncoding) {
                        key = theKey
                    }
                }
                
                let errorLog = error ?? "nil"
                
                print("Verification completionBlock: verified=\(verified); userKey=\(key); error=\(errorLog)")
                
                if !verified && (error?.code == EVInvalidLicenseError) {
                    //                        self.performSegueWithIdentifier("license", sender:self)
                }
                
                if (error == nil) {
                    self.doSegueForResult(verified, error: error)
                }
            })
        }
    }

    func doSegueForResult(verified: Bool, error: NSError?) {
        if verified {
            if let player = verifiedPrompt {
                player.play()
            }
            if let demoMode = NSUserDefaults.standardUserDefaults().objectForKey("demoMode") as? Bool {
                weak var weakSelf: VerifyViewController? = self
                dispatch_async(dispatch_get_main_queue()) {
                    if let ws = weakSelf {
                        ws.performSegueWithIdentifier(demoMode ? "demo" : "success", sender: ws)
                    }
                }
            } else {
                weak var weakSelf: VerifyViewController? = self
                dispatch_async(dispatch_get_main_queue()) {
                    if let ws = weakSelf {
                        ws.performSegueWithIdentifier("success", sender: ws)
                    }
                }
            }
        } else {
            weak var weakSelf: VerifyViewController? = self
            dispatch_async(dispatch_get_main_queue()) {
                if let ws = weakSelf {
                    ws.performSegueWithIdentifier("failure", sender: ws)
                }
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? VerifyResultViewController {
            if let id = segue.identifier {
                switch id {
                case "success":
                    vc.messageColor = UIColor.blackColor()
                    vc.message = NSLocalizedString("Eyeprint verified!", comment: "")
                case "failure":
                    vc.messageColor = UIColor.redColor()
                    vc.message = NSLocalizedString("Not Verified", comment: "")
                default:
                    print("Warning: This is not the segue you're looking for...")
                }
            }
        }
    }
    
    @IBAction func cancel(sender: AnyObject) {
        if let ev = EyeVerifyLoader.getEyeVerifyInstance() {
            ev.cancel()
            self.performSegueWithIdentifier("unwindToUserViewController", sender:self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func eyeStatusChanged(newEyeStatus: EVEyeStatus) {
        self.scanningOverlay.targetHighlighted = false
        switch newEyeStatus {
        case .NoEye:
            self.notificationLabel.text = NSLocalizedString("Position your eyes in the window", comment: "")
            self.notificationLabel.hidden = false
        case .TooFar:
            self.notificationLabel.text = NSLocalizedString("Move device closer", comment: "")
            self.notificationLabel.hidden = false
        case .Okay:
            self.notificationLabel.hidden = true
            self.scanningOverlay.targetHighlighted = true
        }
    }
}
