//
//  EVStyleSheet.swift
//  EyeVerify
//

import UIKit

class EVStyleSheet {
    var globalTintColor: UIColor?
    var textColor: UIColor?
    var brandColor: UIColor?
    var backgroundColor: UIColor?
    
    func loadStylSheet() {
        if let path = NSBundle.mainBundle().pathForResource("EVStyleSheet", ofType:"plist") {
            if let loadedDictionary = readDictionaryFromFile(path){
                println(loadedDictionary.description)
                if let globalTintHex = loadedDictionary["globalTint"] as? String {
                    self.globalTintColor = UIColor(rgba:globalTintHex)
                }
                if let textColorHex = loadedDictionary["textColor"] as? String {
                    self.textColor = UIColor(rgba:textColorHex)
                }
                if let brandColorHex = loadedDictionary["brandColor"] as? String {
                    self.brandColor = UIColor(rgba:brandColorHex)
                }
                if let backgroundColorHex = loadedDictionary["backgroundColor"] as? String {
                    self.backgroundColor = UIColor(rgba:backgroundColorHex)
                }
            }
        }
    }
    
    func readDictionaryFromFile(filePath:String) -> Dictionary<String,AnyObject!>? {
        var anError : NSError?
        
        let data : NSData! = NSData(contentsOfFile:filePath, options: NSDataReadingOptions.DataReadingUncached, error: &anError)
        
        if let theError = anError{
            return nil
        }
        
        if let dictionary = NSPropertyListSerialization.propertyListWithData(data, options: 0,format: nil, error: &anError) as? NSDictionary {
            var swiftDict : Dictionary<String,AnyObject!> = Dictionary<String,AnyObject!>()
            for key : AnyObject in dictionary.allKeys {
                if let stringKey = key as? String {
                    if let keyValue : AnyObject = dictionary.valueForKey(stringKey){
                        swiftDict[stringKey] = keyValue
                    }
                }
                return swiftDict
            }
        } else if let theError = anError {
            println("Sorry, couldn't read the file \(filePath.lastPathComponent):\n\t"+theError.localizedDescription)
        }
        return nil
    }
}
