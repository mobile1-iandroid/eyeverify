//
//  EVThemeController.swift
//  EyeprintID
//

import UIKit

class EVThemeController: NSObject {

    static var themes: [EVTheme] = []
    
    class var currentTheme: EVTheme? {
        get {
            // Load current theme
            let file = "evtheme.current"
            if let dirs : [String] = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String] {
                let dir = dirs[0] //documents directory
                let path = dir.stringByAppendingPathComponent(file);
                let currentThemeName: String?
#if SWIFT2
//                do {
//                    currentThemeName = try String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
//                } catch _ {
//                    currentThemeName = nil
//                }
#else
                currentThemeName = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)
#endif
                for item in themes {
                    if item.name == currentThemeName {
                        return item
                    }
                }
            }
            return nil
        }
        set {
            let file = "evtheme.current"
            if let dirs : [String] = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String] {
                let dir = dirs[0] //documents directory
                let path = dir.stringByAppendingPathComponent(file);
                if let newValue = newValue {
#if SWIFT2
//                do {
//                    try newValue.name.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding)
//                } catch _ {
//                }
#else
                    newValue.name.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding, error: nil)
#endif
                } else {
#if SWIFT2
//                    do {
//                        try NSFileManager.defaultManager().removeItemAtPath(path)
//                    } catch _ {
//                    }
#else
                    NSFileManager.defaultManager().removeItemAtPath(path, error: nil)
#endif
                }
            }
        }
    }

    class func deleteTheme(theme: EVTheme) {
        if let documents = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as? String {
            let themesPath = documents.stringByAppendingPathComponent("themes")
            print("Deleting theme \(theme.name).evtheme from disk")
            let filePath = themesPath.stringByAppendingPathComponent(theme.name.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))
            let deleteFile = filePath + ".evtheme"
#if SWIFT2
//            do {
//                try NSFileManager.defaultManager().removeItemAtPath(deleteFile)
//            } catch _ {
//            }
#else
            NSFileManager.defaultManager().removeItemAtPath(deleteFile, error: nil)
#endif
            for var index = 0; index < themes.count; ++index {
                if themes[index].name == theme.name {
                    if currentTheme?.name == theme.name {
                        self.currentTheme = nil
                    }
                    self.themes.removeAtIndex(index)
                }
            }
        }
    }
    
    class func saveTheme(theme: EVTheme) {
        if let documents = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as? String {
            let themesPath = documents.stringByAppendingPathComponent("themes")
            if !NSFileManager.defaultManager().fileExistsAtPath(themesPath) {
#if SWIFT2
//            do {
//                try NSFileManager.defaultManager().createDirectoryAtPath(themesPath, withIntermediateDirectories:true, attributes:nil)
//            } catch _ {
//            }
#else
                NSFileManager.defaultManager().createDirectoryAtPath(themesPath, withIntermediateDirectories: true, attributes: nil, error: nil)
#endif
            }
            print("Saving theme \(theme.name).evtheme to disk")
            let filePath = themesPath.stringByAppendingPathComponent(theme.name.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))
            let saveFile = filePath + ".evtheme"
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                NSKeyedArchiver.archiveRootObject(theme, toFile: saveFile)
            }
            self.themes.append(theme)
        }
    }

    class func loadThemes() {
        self.themes = []
        if let documents = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as? String {
            let themesPath = documents.stringByAppendingPathComponent("themes")
#if SWIFT2
//            do {
//                let directoryContents =  try NSFileManager.defaultManager().contentsOfDirectoryAtPath(themesPath)
//                print("Loading themes from disk")
//                for file in directoryContents {
//                    print("Loading \(file)")
//                    if let fileName = file as? String {
//                        let filePath = themesPath.stringByAppendingPathComponent( fileName)
//                        if let theme = NSKeyedUnarchiver.unarchiveObjectWithFile(filePath) as? EVTheme {
//                            self.themes.append(theme)
//                        }
//                    }
//                }
//            } catch _ {
//            }
#else
            if let directoryContents = NSFileManager.defaultManager().contentsOfDirectoryAtPath(themesPath, error: nil) as? [String] {
            for file in directoryContents {
                    let filePath = themesPath.stringByAppendingPathComponent(file)
                    if let theme = NSKeyedUnarchiver.unarchiveObjectWithFile(filePath) as? EVTheme {
                        self.themes.append(theme)
                    }
                }
            }
#endif
        }
    }
}


