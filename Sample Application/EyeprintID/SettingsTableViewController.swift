//
//  SettingsTableViewController.swift
//  EyeprintID
//
//  Created by goodle on 6/14/15.
//  Copyright (c) 2015 EyeVerify. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    @IBOutlet weak var imageLoggingCell: SwitchTableViewCell!
    
    @IBOutlet weak var demoModeCell: SwitchTableViewCell!
    @IBOutlet weak var themeCell: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageLoggingCell?.switchView?.addTarget(self, action: "updateImageLogging", forControlEvents: UIControlEvents.ValueChanged)
        demoModeCell?.switchView?.addTarget(self, action: "updateDemoMode", forControlEvents: UIControlEvents.ValueChanged)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let demoMode = NSUserDefaults.standardUserDefaults().objectForKey("demoMode") as? Bool {
            if demoMode {
                if let theme = EVThemeController.currentTheme {
                    self.navigationController?.navigationBar.tintColor = theme.globalTintColor
                }
            }
        }
        self.imageLoggingCell.switchView?.on = EVSettings.getInstance().logEnhancedImages
        if let demoMode = NSUserDefaults.standardUserDefaults().objectForKey("demoMode") as? Bool {
            self.demoModeCell.switchView?.on = demoMode
        }
        updateDemoMode()
    }
    
    override func viewDidAppear(animated: Bool) {
        self.tableView.setNeedsLayout()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    func updateImageLogging() {
        if imageLoggingCell.switchView?.on == true {
            EVSettings.getInstance().logEnhancedImages = true
        } else {
            EVSettings.getInstance().logEnhancedImages = false
        }
    }
    
    func updateDemoMode() {
        if demoModeCell.switchView?.on == true {
            print("Demo mode enabled")
            themeCell.hidden = false
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "demoMode")
            if let theme = EVThemeController.currentTheme {
                themeCell.detailTextLabel?.text = theme.name
                themeCell.layoutSubviews()
                self.navigationController?.navigationBar.tintColor = theme.globalTintColor
            }
        } else {
            print("Demo mode disabled")
            themeCell.hidden = true
            self.navigationController?.navigationBar.tintColor = nil
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "demoMode")
        }
        NSUserDefaults.standardUserDefaults().synchronize()
    }
}
