//===-- AppDelegate.swift - AppDelegate class ----------------------------===//
//
//                     EyeVerify Codebase
//
//===----------------------------------------------------------------------===//
///
/// @file
/// @brief This file contains the declaration of the AppDelegate class.
///
//===----------------------------------------------------------------------===//

import UIKit
import Fabric
import Crashlytics



enum ExampleMode : Int {
    case Basic // Without user key storage
    case LocalWithoutKeyGeneration // Local without user key generation
    case LocalWithKeyGeneration // Local with user key generation
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?


    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
        // Set license automatically during development
        NSUserDefaults.standardUserDefaults().setObject("eyeverify-demo", forKey: "license")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        var isLicensed = false
        if let license = NSUserDefaults.standardUserDefaults().objectForKey("license") as? String {
            if !license.isEmpty {
                isLicensed = true
                let evLoader = EyeVerifyLoader()
                evLoader.loadEyeVerifyWithLicense(license)
            }
        }
        
        EVThemeController.loadThemes()
        Fabric.with([Crashlytics()])
        return isLicensed ? true : false
    }
   
#if SWIFT2
//    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
//        if url.scheme == "ev-groupid" {
//            if let license = url.query {
//                NSUserDefaults.standardUserDefaults().setObject(license, forKey: "license")
//                NSUserDefaults.standardUserDefaults().synchronize()
//                EyeVerify.setLicense(license)
//            }
//            return true
//        }
//        
//        if let path = url.path {
//            if let documents = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as? String {
//                let themesPath = documents.stringByAppendingPathComponent("themes")
//                if !NSFileManager.defaultManager().fileExistsAtPath(themesPath) {
//                    do {
//                        try NSFileManager.defaultManager().createDirectoryAtPath(themesPath, withIntermediateDirectories:true, attributes:nil)
//                    } catch _ {
//                    }
//                }
//                let filePath = themesPath.stringByAppendingPathComponent(path.lastPathComponent)
//                do {
//                    try NSFileManager.defaultManager().copyItemAtPath(path, toPath: filePath)
//                } catch _ {
//                }
//                EVThemeController.loadThemes()
//                let message = UIAlertView(title: NSLocalizedString("Congrats :)", comment: ""), message: NSLocalizedString("You successfully imported a theme from \(path.lastPathComponent)", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("Sweet as", comment:""))
//                message.show()
//            }
//        }
//        return false
//    }
#else
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        if url.scheme == "ev-groupid" {
            if let license = url.query {
                NSUserDefaults.standardUserDefaults().setObject(license, forKey: "license")
                NSUserDefaults.standardUserDefaults().synchronize()
                let evLoader = EyeVerifyLoader()
                evLoader.loadEyeVerifyWithLicense(license)
            }
            return true
        }
        
        if let path = url.path {
            if let documents = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as? String {
                let themesPath = documents.stringByAppendingPathComponent("themes")
                if !NSFileManager.defaultManager().fileExistsAtPath(themesPath) {
                    NSFileManager.defaultManager().createDirectoryAtPath(themesPath, withIntermediateDirectories: true, attributes: nil, error: nil)
                }
                let filePath = themesPath.stringByAppendingPathComponent(path.lastPathComponent)
                NSFileManager.defaultManager().copyItemAtPath(path, toPath: filePath, error: nil)
                EVThemeController.loadThemes()
                let message = UIAlertView(title: NSLocalizedString("Congrats :)", comment: ""), message: NSLocalizedString("You successfully imported a theme from \(path.lastPathComponent)", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("Sweet as", comment:""))
                message.show()
            }
        }
        return false
    }
#endif
}
