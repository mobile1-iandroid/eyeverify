//
//  EnrollViewController.swift
//  EyeprintID
//

import UIKit

class EnrollViewController: UIViewController, EVAuthSessionDelegate {
    
    @IBOutlet weak var scanAgainButton: UIButton!
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var scanningOverlay: ScanningOverlayView!
    @IBOutlet weak var counterText: UILabel!
    @IBOutlet weak var progressBarView: UIView!
    @IBOutlet weak var progressBarWidthConstraint: NSLayoutConstraint!
    var enrollmentComplete = false
    var shouldDismiss = false
    
    let DemoUserKey = "iOS User Key Test 1!2@3#4$5%6^7&8*9/0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let ev = EyeVerifyLoader.getEyeVerifyInstance() {
            if let username =  NSUserDefaults.standardUserDefaults().objectForKey("username") as? String {
                if username.isEmpty {
                    self.shouldDismiss = true
                }
                ev.userName = username
                ev.setCaptureView(self.captureView)
            } else {
                self.shouldDismiss = true
            }
        } else {
            self.shouldDismiss = true
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.counterText.hidden = true
        self.scanAgainButton.hidden = true
        
        if let demoMode = NSUserDefaults.standardUserDefaults().objectForKey("demoMode") as? Bool {
            if demoMode {
                if let theme = EVThemeController.currentTheme {
                    self.cancelButton.tintColor = theme.globalTintColor
                    self.navigationController?.navigationBar.tintColor = theme.globalTintColor
                }
            } else {
                self.navigationController?.navigationBar.tintColor = nil
                self.cancelButton.tintColor = nil
            }
        }
        self.progressBarWidthConstraint.constant = 0;
    }

    override func viewDidAppear(animated: Bool) {
        if self.shouldDismiss {
            self.performSegueWithIdentifier("unwindToUserViewController", sender: self)
            return
        }
        if !enrollmentComplete {
            self.enroll()
        }
    }
    
    func enroll() {
        if let ev = EyeVerifyLoader.getEyeVerifyInstance() {
            ev.setEVAuthSessionDelegate(self)
            ev.enrollUser(ev.userName, userKey:DemoUserKey.dataUsingEncoding(NSUTF8StringEncoding), localCompletionBlock: { (enrolled, userKey, error) in
                print("Enrollment localCompletionBlock: enrolled=\(enrolled); userKey=\(userKey != nil ? NSString(data: userKey!, encoding: NSUTF8StringEncoding) : nil) error=\(error)")
                self.enrollmentComplete = true
                weak var weakSelf: EnrollViewController? = self
                dispatch_async(dispatch_get_main_queue()) {
                    if let ws = weakSelf {
                        if (enrolled) {
                            ws.performSegueWithIdentifier("success", sender: self)
                        } else {
                            ws.performSegueWithIdentifier("failure", sender: self)
                        }
                    }
                }
            })
        }
    }
    
    @IBAction func cancel(sender: AnyObject) {
        if let ev = EyeVerifyLoader.getEyeVerifyInstance() {
            ev.cancel()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func scanAgain(sender: AnyObject) {
        if let ev = EyeVerifyLoader.getEyeVerifyInstance() {
            ev.continueAuth()
        }
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func eyeStatusChanged(newEyeStatus: EVEyeStatus) {
        self.scanningOverlay.targetHighlighted = false
        switch newEyeStatus {
        case .NoEye:
            self.notificationLabel.text = NSLocalizedString("Position your eyes in the window", comment: "")
            self.notificationLabel.hidden = false
        case .TooFar:
            self.notificationLabel.text = NSLocalizedString("Move device closer", comment: "")
            self.notificationLabel.hidden = false
        case .Okay:
            self.scanningOverlay.targetHighlighted = true
            self.notificationLabel.hidden = true
        }
    }
    
    func enrollmentProgressUpdated(completionRatio: Float, counter counterValue: Int32)
    {
        self.counterText.text =  String(counterValue)
        self.counterText.hidden = (counterValue == 0)

        self.progressBarWidthConstraint.constant = self.progressBarView.bounds.width * CGFloat(completionRatio);
        self.progressBarView.layoutSubviews()
    }
    
    func enrollmentSessionStarted(totalSteps: Int32) {
        self.counterText.text =  String(totalSteps)
        self.counterText.hidden = false
        self.scanAgainButton.hidden = true
        self.scanningOverlay.hidden = false
    }
    
    func enrollmentSessionCompleted() {
        self.counterText.hidden = true
        self.scanAgainButton.hidden = false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let vc = segue.destinationViewController as? EnrollResultViewController {
            if segue.identifier == "success" {
                vc.messageColor = UIColor.blackColor()
                vc.message = NSLocalizedString("Enrolled Successfully!", comment: "")
            } else if segue.identifier == "failure" {
                vc.messageColor = UIColor.redColor()
                vc.message = NSLocalizedString("Enrollment Unsuccessful", comment: "")
            }
        }
    }
}
